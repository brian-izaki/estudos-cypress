# Estudos sobre Cypress
Framework de testes E2E (End to End) conceitos básicos

## Teoria
- Testes 
  - **Unitários**: pega apenas uma parte isolada do código (devem ser rápidos).
  - **Integração**: irá unir alguns componentes e testar o comportamento em conjunto (retornos de API, verificações, mocks).
    - roda em um sandBox.
  - **End to end**: servem para validar a aplicação inteira (do início ao fim), testa uma página inteira (como se fosse um usuário de verdade), ele não será tão rapido.
    - é necessário rodar dentro de uma engine de browser.

- Como testar?
  - pegue um fluxo importante da aplicação.
  - separe um passo-a-passo das ações que o usuário faria na aplicação.
  - para cada passo, teste o que precisa ocorrer.
  - pensar no "caminho feliz" e depois nos "casos falhos".

- O Cypress
  - ele vem com várias dependencias, lib de assertion (**verificações**), com mocking, e stubbing.
  - não utiliza o Selenium.
  - escrito apenas em JS.
  - funciona com qualquer Framework JS ou site (HTML puro)
  - as lib que ele vem, é possível utilizar os métodos que pertencem a elas, ex: mocka.

## Inicio do projeto
1. `npm init`
2. `npm install cypress --dev`
3. Na primeira vez deve executar `npx cypress open`
4. Abre aplicação electron (automaticamente) e mostra exemplos de testes automatizados.
5. Ele cria uma estrutura de pastas no projeto.

# Executando o projeto depois do clone
1. clonar esse repositório
2. executar no terminal com o caminho desse diretório: `npm install`
3. executar os testes em modo **headless**: `npm test` ou `npx cypress run`
4. executar os testes vendo a **GUI**: `npx cypress open`

## Estrutura de pastas
- `cypress/integrations`: ficam os testes que o cypres irá executar.
- `cypress/support/commands.js`: neste arquivo ficarão as funções que são comandos customizados para o `cy`, ou seja, as funções que são criadas aqui podem ser utilizadas como métodos pelo `cy`.

## Funções utilizadas
- `it('descrição', callback)`: executa um teste.
  - `.only`: é um método que permite executar somente o `it` com ele, independente se tiver outros `it`, caso outros tenham este método, tbm serão executados.
- `beforeEach(callback)`: irá realizar as ações da callback antes de cada teste "`it`"
- `cy.get('elemento')`: identifica um elemento
  - `as('nome-do-alias')`: cria um nome para o elemento que foi passado no `cy.get`, com ele não será preciso mais passar o caminho desse elemento, basta utilizar o `'@nome-do-alias'`, o `@` é a forma que o cypress sabe que o caminho do elemento é um alias. O alias salva o elemento daquele momento, ou seja caso haja alterações após ser salvo, o alias não sofre alterações.
  - `.type('será digitado')`: método que permite digitar em um input.
  - `.select('valor dos options')`: seleciona uma opção dos `options` que ficam dentro de um **`select`**.
  - `.check()`: seleciona o **radio button** que o `cy.get()` pegou.
  - `.should('string padrão', 'valor que deve ter')`: realiza o assertion (verificação) para validar se determinado local possui o valor que é esperado de acordo com a `string padrão` que já existe para diversos casos (veja documentação).

# Comando customizado
- ajudam a reaproveitar testes que se repetem no ciclo de vida da aplicação (ex: login, preencher campos de nomes, etc)
- necessário ir no arquivo `support/commands.js` e criar uma função.
- haverá exemplos de como criar uma nos cometários.

# Modo Headless
- rodar o projeto em **modo headless** 
  - deve executar ele no terminal, ele é mais rápido que o feito pela GUI.
  - ao final dos testes ele irá gerar um diretório chamado video, na qual ele mostra a gravação dos testes feitos.

# Continous Integration (CI)
- utilizado o **gitLab** pois ele fornece o suporte para esta parte do desenvolvimento de projetos.
- será executado o modo headless para os testes.
- por estarmos utilizando o gitlab, é necessário criar o arquivo `.gitlab-ci.yml`, ele será o responsável pela configuração.
  ```yml
  image: 'cypress/base'

  stages:
    - e2e

  end-to-end testing:
    stage: e2e
    script:
      - npm install
      - npm test
  ```
  - **image**: pega uma **imagem docker** do docker hub para executar os testes.
  - **stages**: se refere aos estágios que será executado (nesse caso existe apenas o end-to-end).
  - **end-to-end testing**: ele é o nosso _job_, ele que realiza as ações.
    - _stage_: diz a qual estágio o job faz parte
    - _script_: diz quais scripts o job irá rodar na aplicação. (toma como base de que é a primeira vez que a aplicação irá rodar, uma analogia seria como se fosse o clone do projeto)

## O Cypress
- pegando elementos
  - é possível pegar elementos utilizado seletores (como se fosse utilizar o `document.querySelector`)

## Referências: 
- video de [introdução do willian justen](https://www.udemy.com/course/cypress-na-pratica/)
- curso testes automatizados: [básico com cypress do walmyr Filho](https://www.udemy.com/course/testes-automatizados-com-cypress-basico/)
- Cypress: 
  - [doc da API](https://docs.cypress.io/api/table-of-contents): possui os métodos que possuem no cypress.

