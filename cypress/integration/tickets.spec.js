describe("Tickets", () => {
  // irá realizar antes de cada teste
  beforeEach(() => cy.visit("http://bit.ly/2XSuwCW"));

  it("fills all the text input fields", () => {
    const firstName = "Rimuru";
    const lastName = "Tempest";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("rimuru@gmail.com");
    cy.get("#requests").type("qualquer comida serve");
    cy.get("#signature").type(`${firstName} ${lastName}`);
  });

  it("select two tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("select 'vip' radioButton", () => {
    cy.get("#vip").check();
  });

  it("select 'social media' checkbox", () => {
    cy.get("#social-media").check();
  });

  it("select 'friend' and 'publication', then uncheck 'friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });

  // realiza o teste
  it("has 'TICKETBOX' header's heading", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("alerts on invalid email", () => {
    cy.get("#email").as("email").type("teste-gmail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email").clear().type("teste@gmail.com");

    cy.get("#email.invalid").should("not.exist");
  });

  it("fills and reset the form", () => {
    const firstName = "Rimuru";
    const lastName = "Tempest";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("rimuru@gmail.com");
    cy.get("#ticket-quantity").select("3");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#social-media").check();
    cy.get("#requests").type("vegetarian");
    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 3 VIP tickets.`
    );
    cy.get("#agree").click();
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");
    cy.get("button[type='reset']").click();
    cy.get("@submitButton").should("be.disabled");
  });

  it("fills mandatory fields using support command", () => {
    const customer = {
      firstName: "João",
      lastName: "Silva",
      email: "joaosilva@example.com"
    }

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");

  })
});
